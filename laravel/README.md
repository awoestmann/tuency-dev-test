# Laravel

Build image:

~~~bash
docker build -t tuency/laravel .
~~~

Run container:

~~~bash
docker run --name tuency_laravel -p 18080:8080 --net=tuency_network -v $PWD:/opt/workdir -d tuency/laravel
~~~

This maps host port 18080 to container port 8080, which can be used to connect to laravel app. The current folder is mounted as a volume in the container.

The laravel app may require an installation after the container has been started
~~~bash
docker exec -ti tuency_laravel bash
cd testapp
composer install
~~~
