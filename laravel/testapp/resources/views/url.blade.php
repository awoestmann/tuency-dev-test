<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>URL Page</title>
    </head>
    <body class="antialiased">
            <div>
                URL:
                {{ $url }}
            </div>
    </body>
</html>
