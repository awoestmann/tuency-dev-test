#!/bin/bash

apt -y install --no-install-recommends wget gnupg ca-certificates
wget -qO - https://openresty.org/package/pubkey.gpg | apt-key add -
echo "deb http://openresty.org/package/debian buster openresty" \
    | tee /etc/apt/sources.list.d/openresty.list
apt update
apt -y install openresty

apt -y install luarocks
luarocks install lua-cjson
luarocks install lua-resty-openidc