# Client

Build image:

~~~bash
docker build -t tuency/client .
~~~

Run container:

~~~bash
docker run --name client -p 18081:80 --net=tuency_network -v $PWD:/opt/workdir -d tuency/client
~~~

This maps host port 18081 to container port 8080, which can be used to connect to the webserver. The current folder is mounted as a volume in the container.

The example host names can be added to the container's hosts file for testing purposes.

~~~bash
docker exec -ti tuency_client bash
echo -e "\n127.0.0.1 anExample.local\n127.0.0.1 anotherExample.local" >> /etc/hosts
~~~

To use the host machines browser, the container ip adresses can for example be added to the hosts file

~~~bash
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_client)" "anExample.local" >> /etc/hosts
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_client)" "anotherExample.local" >> /etc/hosts
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_keycloak)" "tuency_keycloak" >> /etc/hosts
~~~
