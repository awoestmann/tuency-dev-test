# Keycloak

Build image

~~~bash
docker build -t tuency/keycloak .
~~~

Start container

~~~bash
docker run --name tuency_keycloak --net=tuency_network -p 28080:8080 -d tuency/keycloak 
~~~
This maps host port 28080 to container port 8080, which can be used to connect to the Wildfly/Keycloak

Start a bash inside the container and add an admin user
~~~bash
docker exec -ti tuency_keycloak bash
cd bin
./add-user-keycloak.sh -u admin -p secret
./jboss-cli.sh --connect command=:reload
~~~

Connect to keycloak using http://localhost:28080/auth/ on the host machine.
